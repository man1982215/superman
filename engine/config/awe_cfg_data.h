/**
 * @File Name     : awe_cfg_data.h
 * @Author        : wangyang
 * @Record        :
 * @Date          : 2023-03-18
 * @Version       : 1.0
 * @Function List :
 * @Copyright (C), 2023, 上海艾拉比智能科技有限公司.                ****
 * @Modification  : Created file

*/
#ifndef __AWE_CFG_DATA_H__
#define __AWE_CFG_DATA_H__


#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */

/** ************************************LOG CONFIG BEGIN*************************************************************** */
/** 对象key定义 */
#define AWE_CFG_OWNER_LOG       "LOG"       /** <OWNER>    LOG */
#define AWE_CFG_LOG_FILE_PATH          AWE_CFG_OWNER_LOG":logFilePath"          /** 日志存储路径 */
#define AWE_CFG_LOG_FILE_SIZE_MAX      AWE_CFG_OWNER_LOG":logFileSizeMax"       /** 单个日志文件大小最大值 */
#define AWE_CFG_LOG_FILE_NUM_MAX       AWE_CFG_OWNER_LOG":logFileNumMax"    	/** 日志文件数量最大值 */
#define AWE_CFG_LOG_COLOR_FLAG		   AWE_CFG_OWNER_LOG":logColorFlag"   		/** 日志打印颜色显示使能标记 */
#define AWE_CFG_LOG_LEVEL			   AWE_CFG_OWNER_LOG":logLevel"   			/** 日志等级 */

/** 缺省值定义 */
#define AWE_CFG_LOG_FILE_PATH_DEFAULT			"/tmp/awd/log/awa_app.log"	/** 日志存储路径缺省值 */
#define AWE_CFG_LOG_FILE_SIZE_MAX_DEFAULT		(5*1024*1024) 				/** 单个日志文件大小最大值缺省值（5M） */
#define AWE_CFG_LOG_FILE_NUM_MAX_DEFAULT		5							/** 日志文件数量最大值缺省值（5个，不包含正在写入的文件） */
#define AWE_CFG_LOG_COLOR_FLAG_DEFAULT			0							/** 日志打印颜色显示使能标记缺省值（关闭） */
#define AWE_CFG_LOG_LEVEL_DEFAULT				3							/** 日志等级缺省值（3：info） */

/** ************************************LOG CONFIG END*************************************************************** */

#if 0
#endif

/** ************************************SQL CONFIG BEGIN*************************************************************** */
/** 对象key定义 */
#define AWE_CFG_OWNER_SQL       "SQL"       /** <OWNER>    SQL */
#define AWE_CFG_SQL_FILE_PATH          AWE_CFG_OWNER_SQL":dbFilePath"   /** 数据库文件存储路径 */

/** 缺省值定义 */
#define AWE_CFG_SQL_FILE_PATH_DEFAULT			"/tmp/awd/db/AWE_.db"	/** 数据库文件存储路径缺省值 */

/** ************************************SQL CONFIG END*************************************************************** */

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __AWE_CFG_DATA_H__ */
