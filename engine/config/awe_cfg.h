/**
 * @File Name     : awe_cfg.h
 * @Author        : wangyang
 * @Record        :
 * @Date          : 2023-03-18
 * @Version       : 1.0
 * @Function List :
 * @Copyright (C), 2023, 上海艾拉比智能科技有限公司.                ****
 * @Modification  : Created file

*/
#ifndef __AWE_CFG_H__
#define __AWE_CFG_H__

#include"awf_common.h"
#include"dictionary.h"
#include"iniparser.h"
#include"awe_cfg_data.h"

#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */

typedef struct _dictionary_ Dictionary_t;

/**
 * @brief       ini配置初始化
 * @param  in   CHAR *pcFilePath  配置文件绝对路径
 * @retval      INT
 * @date        2023-03-18
 * @author      wangyang
 */
extern INT AWE_CFG_Init(CHAR *pcFilePath);

/**
 * @brief       ini配置获取字符串对象
 * @param  in   const CHAR *pcKey         	对象key
 * @param  in   const CHAR *pcDefaultVal  	缺省值
 * @retval      CHAR *						对象对应的值
 * @date        2023-03-18
 * @author      wangyang
 */
extern CHAR *AWE_CFG_iniParserString(const CHAR *pcKey, const CHAR *pcDefaultVal);

/**
 * @brief       ini配置获取整型对象
 * @param  in   const CHAR *pcKey  	对象key
 * @param  in   INT iDefaultVal    	缺省值
 * @retval      INT					对象对应的值
 * @date        2023-03-18
 * @author      wangyang
 */
extern INT AWE_CFG_iniParserInt(const CHAR *pcKey, INT iDefaultVal);

/**
 * @brief       ini配置获取布尔型对象
 * @param  in   const CHAR *pcKey  	对象key
 * @param  in   BOOL bDefaultVal   	缺省值
 * @retval      BOOL				对象对应的值
 * @date        2023-03-18
 * @author      wangyang
 */
extern BOOL AWE_CFG_iniParserBoolean(const CHAR *pcKey, BOOL bDefaultVal);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __AWE_CFG_H__ */
