/**
 * @File Name     : awe_cfg.c
 * @Author        : wangyang
 * @Record        :
 * @Date          : 2023-03-18
 * @Version       : 1.0
 * @Function List :
 * @Copyright (C), 2023, 上海艾拉比智能科技有限公司.                ****
 * @Modification  : Created file

 * @Description   : ini配置处理源文件
*/
#include"awe_cfg.h"
#include"awe_cfg_data.h"


#define LOG_TAG      "CFG"

static Dictionary_t* g_dict = NULL;

/**
 * @brief       ini配置初始化
 * @param  in   CHAR *pcFilePath  配置文件绝对路径
 * @retval      INT
 * @date        2023-03-18
 * @author      wangyang
 */
INT AWE_CFG_Init(CHAR *pcFilePath)
{	
	g_dict = iniparser_load(pcFilePath);
	if (NULL == g_dict)
	{
		printf("load failed\r\n");
		return ERROR_CODE_INI_LOAD_FAILED;
	}

	printf("load success\r\n");
	
	return ERROR_CODE_OK;
}

/**
 * @brief       ini配置获取字符串对象
 * @param  in   const CHAR *pcKey         	对象key
 * @param  in   const CHAR *pcDefaultVal  	缺省值
 * @retval      CHAR *						对象对应的值
 * @date        2023-03-18
 * @author      wangyang
 */
CHAR *AWE_CFG_iniParserString(const CHAR *pcKey, const CHAR *pcDefaultVal)
{
    CHAR *pcRealVal = NULL;

    do 
    {
        pcRealVal = pcDefaultVal;
        if (NULL == g_dict) 
        {
            log_e("g_dict is nil, please call AWE_CFG_iniParserString to init ini config");
            break;
        }

        pcRealVal = iniparser_getstring(g_dict, pcKey, pcDefaultVal);
        log_i("[%s = %s]", pcKey, pcRealVal);
    } while(0);

    return pcRealVal;
}

/**
 * @brief       ini配置获取整型对象
 * @param  in   const CHAR *pcKey  	对象key
 * @param  in   INT iDefaultVal    	缺省值
 * @retval      INT					对象对应的值
 * @date        2023-03-18
 * @author      wangyang
 */
INT AWE_CFG_iniParserInt(const CHAR *pcKey, INT iDefaultVal)
{
    INT iRealVal = 0;

    do 
    {
        iRealVal = iDefaultVal;
        if (NULL == g_dict) 
        {
            log_e("g_dict is nil, please call AWE_CFG_iniParserInt to init ini config");
            break; 
        }

        iRealVal = iniparser_getint(g_dict, pcKey, iDefaultVal);
        log_i("[%s = %d]", pcKey, iRealVal);
    } while(0);

    return iRealVal;
}

/**
 * @brief       ini配置获取布尔型对象
 * @param  in   const CHAR *pcKey  	对象key
 * @param  in   BOOL bDefaultVal   	缺省值
 * @retval      BOOL				对象对应的值
 * @date        2023-03-18
 * @author      wangyang
 */
BOOL AWE_CFG_iniParserBoolean(const CHAR *pcKey, BOOL bDefaultVal)
{
    BOOL bRealVal = 0;

    do 
    {
        bRealVal = bDefaultVal;
        if (NULL == g_dict) 
		{
           log_e("g_dict is nil, please call AWE_CFG_iniParserBoolean to init ini config");
            break; 
        }

        bRealVal = iniparser_getboolean(g_dict, pcKey, bDefaultVal);
        log_i("[%s = %d]", pcKey, bRealVal);
    } while(0);

    return bRealVal;
}
