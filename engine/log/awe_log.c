/**
 * @File Name     : awe_log.c
 * @Author        : wangyang
 * @Record        :
 * @Date          : 2023-03-18
 * @Version       : 1.0
 * @Function List :
 * @Copyright (C), 2023, 上海艾拉比智能科技有限公司.                ****
 * @Modification  : Created file

 * @Description   : 日志模块
*/
#include"awf_common.h"
#include"awe_cfg.h"


#define LOG_TAG      "LOG"

/**
 * @brief       日志初始化
 * @param  in   VOID  
 * @retval      INT
 * @date        2023-03-18
 * @author      wangyang
 */
INT AWE_LOG_Init(VOID)
{
    UCHAR *pucFilePath = NULL;
    INT iFileSizeMax = 0;
    INT iFileNumMax = 0;
    INT iLogLevel = 0;
    BOOL bIsEnableColor = false;

	/* close printf buffer */
	setbuf(stdout, NULL);

	/** 获取日志绝对路径 */
    pucFilePath = AWE_CFG_iniParserString(AWE_CFG_LOG_FILE_PATH, AWE_CFG_LOG_FILE_PATH_DEFAULT);

	/** 获取单个日志文件大小 */
    iFileSizeMax = AWE_CFG_iniParserInt(AWE_CFG_LOG_FILE_SIZE_MAX, AWE_CFG_LOG_FILE_SIZE_MAX_DEFAULT);

	/** 获取日志文件最大数量 */
    iFileNumMax = AWE_CFG_iniParserInt(AWE_CFG_LOG_FILE_NUM_MAX, AWE_CFG_LOG_FILE_NUM_MAX_DEFAULT);

	/** 获取日志打印等级 */
    iLogLevel = AWE_CFG_iniParserInt(AWE_CFG_LOG_LEVEL, AWE_CFG_LOG_LEVEL_DEFAULT);

	/** 获取日志打印颜色使能开关 */
    bIsEnableColor = AWE_CFG_iniParserBoolean(AWE_CFG_LOG_COLOR_FLAG, AWE_CFG_LOG_COLOR_FLAG_DEFAULT);

	/** 初始化 EasyLogger */
	elog_init(iLogLevel);
		
#ifdef ELOG_FILE_ENABLE
	elog_file_init(pucFilePath, iFileSizeMax, iFileSizeMax);
#endif

	/* set EasyLogger log format */
	elog_set_fmt(ELOG_LVL_ASSERT, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
	elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
	elog_set_fmt(ELOG_LVL_WARN, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO & ~ELOG_FMT_T_INFO);
	elog_set_fmt(ELOG_LVL_INFO, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO & ~ELOG_FMT_T_INFO);
	elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO & ~ELOG_FMT_T_INFO);
	elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO & ~ELOG_FMT_T_INFO);
		
#ifdef ELOG_COLOR_ENABLE
	elog_set_text_color_enabled(bIsEnableColor);
#endif
	
	elog_start();

    return ERROR_CODE_OK;
}
