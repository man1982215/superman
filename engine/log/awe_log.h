/**
 * @File Name     : awe_log.h
 * @Author        : wangyang
 * @Record        :
 * @Date          : 2023-03-18
 * @Version       : 1.0
 * @Function List :
 * @Copyright (C), 2023, 上海艾拉比智能科技有限公司.                ****
 * @Modification  : Created file

*/
#ifndef __AWE_LOG_H__
#define __AWE_LOG_H__

#include"elog.h"
#include"elog_file_cfg.h"
#include"elog_cfg.h"

#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */

/**
 * @brief       日志初始化
 * @param  in   VOID  
 * @retval      INT
 * @date        2023-03-18
 * @author      wangyang
 */
extern INT AWE_LOG_Init(VOID);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __AWE_LOG_H__ */
