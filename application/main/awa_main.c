/**
 * @File Name     : awa_main.c
 * @Author        : wangyang
 * @Record        :
 * @Date          : 2023-04-14
 * @Version       : 1.0
 * @Function List :
 * @Copyright (C), 2023, 上海艾拉比智能科技有限公司.                ****
 * @Modification  : Created file

 * @Description   : 主入口
*/

#include"awf_common.h"

