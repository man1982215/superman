/**
 * @File Name     : awf_common.h
 * @Author        : wangyang
 * @Record        :
 * @Date          : 2023-03-17
 * @Version       : 1.0
 * @Function List :
 * @Copyright (C), 2023, 上海艾拉比智能科技有限公司.                ****
 * @Modification  : Created file

*/
#ifndef __AWF_COMMON_H__
#define __AWF_COMMON_H__

#ifdef __cplusplus
extern "C"{

#endif /* __cplusplus */

#include<sys/types.h>
#include<sys/socket.h>  
#include<sys/time.h>
#include<sys/stat.h>			// 获取文件属性
#include<netinet/in.h>  
#include<arpa/inet.h>  
#include<errno.h>  
#include<unistd.h>  			// unix 系统服务函数定义
#include<dirent.h>				// unix类目录操作的头文件
#include<termios.h>				// 终端控制定义
#include<fcntl.h>				// 文件控制定义
#include<netdb.h>
#include<ctype.h> 
#include<stdio.h>  
#include<string.h>  
#include<stdlib.h>				// 标准C  库		
#include<stdbool.h>
#include<pthread.h>
#include<linux/input.h>
#include<linux/netlink.h>
#include<sys/poll.h>
#include<sys/ioctl.h>

#include"awf_types.h"
#include"awf_error.h"

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __AWF_COMMON_H__ */
