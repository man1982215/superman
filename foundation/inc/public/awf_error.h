/**
 * @File Name     : awf_error.h
 * @Author        : wangyang
 * @Record        :
 * @Date          : 2023-03-18
 * @Version       : 1.0
 * @Function List :
 * @Copyright (C), 2023, 上海艾拉比智能科技有限公司.                ****
 * @Modification  : Created file

*/
#ifndef __AWF_ERROR_H__
#define __AWF_ERROR_H__


#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */

typedef enum ERROR_CODE
{
	ERROR_CODE_OK = 0,
	ERROR_CODE_FAILED,
	ERROR_CODE_INVALID_PARAM,

	ERROR_CODE_INI_BEGIN = 0X1001,		
	ERROR_CODE_INI_LOAD_FAILED,
	ERROR_CODE_INI_END = 0X2001,

	ERROR_CODE_MAX
}ERROR_CODE_E;


#define RET_IS_ERR(iRet)        (ERROR_CODE_OK != (iRet))
#define RET_IS_SUCCESS(iRet)    (ERROR_CODE_OK == (iRet))
#define STR_IS_NULL(pstCb)      (NULL == (pstCb))

#define UNUSED_ARG(x)       ((void) x)
#define PRINT_STR(s)        ((NULL != s) ? (s) : " ")

#define FUNC_ENTRY()     	log_i("Entry...")
#define FUNC_EXIT()      	log_i("Exit ...")

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __AWF_ERROR_H__ */
