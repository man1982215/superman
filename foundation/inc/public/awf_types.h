/**
 * @File Name     : awf_types.h
 * @Author        : wangyang
 * @Record        :
 * @Date          : 2023-03-17
 * @Version       : 1.0
 * @Function List :
 * @Copyright (C), 2023, 上海艾拉比智能科技有限公司.                ****
 * @Modification  : Created file

*/
#ifndef __AWF_TYPES_H__
#define __AWF_TYPES_H__


#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */

#ifndef LONG
typedef signed long long LONG; ///< 64bit integer (signed)
#endif

#ifndef FLOAT
typedef float FLOAT; ///< float
#endif

#ifndef DOUBLE
typedef double DOUBLE; ///< double
#endif

#ifndef INT
typedef int INT; ///< 32bit integer
#endif

#ifndef UINT
typedef unsigned int UINT; ///< 32bit integer (unsigned)
#endif

#ifndef VOID
typedef void VOID; ///< 32bit integer
#endif

#ifndef CHAR
typedef char CHAR; ///< 32bit integer
#endif

#ifndef UCHAR
typedef unsigned char UCHAR; ///< 32bit integer
#endif

#ifndef BOOL
typedef bool BOOL; ///< 32bit integer
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __AWF_TYPES_H__ */
